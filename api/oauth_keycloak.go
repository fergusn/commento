package main

import (
	"os"

	"golang.org/x/oauth2"
)

var keycloakConfig *oauth2.Config

func keycloakOauthConfigure() error {
	keycloakConfig = nil
	if os.Getenv("KEYCLOAK_KEY") == "" && os.Getenv("KEYCLOAK_SECRET") == "" {
		return nil
	}

	if os.Getenv("KEYCLOAK_KEY") == "" {
		logger.Errorf("COMMENTO_KEYCLOAK_KEY not configured, but COMMENTO_KEYCLOAK_SECRET is set")
		return errorOauthMisconfigured
	}

	if os.Getenv("KEYCLOAK_SECRET") == "" {
		logger.Errorf("COMMENTO_KEYCLOAK_SECRET not configured, but COMMENTO_KEYCLOAK_KEY is set")
		return errorOauthMisconfigured
	}

	logger.Infof("loading keycloak OAuth config")

	keycloakConfig = &oauth2.Config{
		RedirectURL:  os.Getenv("ORIGIN") + "/api/oauth/keycloak/callback",
		ClientID:     os.Getenv("KEYCLOAK_KEY"),
		ClientSecret: os.Getenv("KEYCLOAK_SECRET"),
		Scopes: []string{
			"profile",
			"email",
		},
		Endpoint: oauth2.Endpoint{
			AuthURL:  os.Getenv("KEYCLOAK_REALM") + "/protocol/openid-connect/auth",
			TokenURL: os.Getenv("KEYCLOAK_REALM") + "/protocol/openid-connect/token",
		},
	}

	configuredOauths = append(configuredOauths, "keycloak")

	return nil
}
