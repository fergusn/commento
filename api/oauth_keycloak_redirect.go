package main

import (
	"fmt"
	"net/http"
)

func keycloakRedirectHandler(w http.ResponseWriter, r *http.Request) {
	if keycloakConfig == nil {
		logger.Errorf("keycloak oauth access attempt without configuration")
		fmt.Fprintf(w, "error: this website has not configured gitlab OAuth")
		return
	}

	commenterToken := r.FormValue("commenterToken")

	_, err := commenterGetByCommenterToken(commenterToken)
	if err != nil && err != errorNoSuchToken {
		fmt.Fprintf(w, "error: %s\n", err.Error())
		return
	}

	url := keycloakConfig.AuthCodeURL(commenterToken)
	http.Redirect(w, r, url, http.StatusFound)
}
